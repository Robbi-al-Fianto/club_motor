-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 21 Jun 2022 pada 08.17
-- Versi server: 10.4.14-MariaDB
-- Versi PHP: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `clubmotor`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `artikel`
--

CREATE TABLE `artikel` (
  `id` int(11) NOT NULL,
  `judul` text NOT NULL,
  `photo` text NOT NULL,
  `content` text NOT NULL,
  `author` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `artikel`
--

INSERT INTO `artikel` (`id`, `judul`, `photo`, `content`, `author`) VALUES
(2, 'Peluncuran Honda PCX Terbaru Semakin Dekat', 'pcx.jpg', 'Liputan6.com, Jakarta - Honda PCX terbaru mulai berani menampakan diri meski masih berbalut kamufkase. Jika sudah begini, waktu peluncuran artinya sudah semakin dekat.\r\n\r\nBaru-baru ini, media massa Thailand berkesempatan berjumpa langsung dengan Honda PCX 160. Kabarnya, skutik premium andalan Honda itu bakal dirilis pada Kamis, 7 Januari 2021.\r\n\r\nMemang belum banyak informasi yang didapatkan dari kegiatan tersebut. Meski demikian, beberapa sumber warta di Negeri Gajah Putih mengeluarkan serangkai gambar di fasilitas milik Honda.\r\n\r\nTajuk acaranya mencoba motor tanpa melihat wujud jelas. Ya, semuanya masih dibungkus kamuflase. Lengkap sampai ke box filter, CVT (Continuously Variable Transmission), fork depan, bahkan kaliper rem. Hanya tulisan eSP diekspos.\r\n\r\nNamun jelas, tanpa harus menerka kami pun tahu. Ini merupakan sosok Honda PCX 160 baru. Yang sebelumnya sudah meluncur duluan di Jepang. Lengkap beserta spesifikasi fitur dan mesin. Dari bungkusan dan siluet motor, hampir sembilan puluh persen unit yang sama ada di Thailand.\r\n\r\nSejauh ini ada beberapa hal yang bisa disimpulkan. Pertama, model jok masih serupa seri lama. Tapi ada sedikit aksen pembaruan. Lapisan kulit hitam kini dipadu coklat pada area penumpang. Lantas tiap partisinya diberi jahitan yang tampak apik.\r\n\r\nPanel instrumen juga sesuai perkiraan sebelumnya. Honda mengadopsi model PCX yang beredar di Jepang atau Eropa. Tampilannya berubah. Jauh lebih modern sekaligus elegan.\r\n\r\nBukan lagi mengandalkan display biru seperti seri lama. Sementara rem depan, dipastikan menggunakan kaliper dua piston. Sementara di sana memakai bungkusan ban Michelin. Menarik.\r\n\r\nRasanya untuk sisa teknologi tak perlu banyak ditebak lagi. Meski bisa saja ada yang dipangkas khusus pasar Asia Tenggara, demi menekan harga. Atau, pembagian varian jadi lebih banyak dari pada sekarang.\r\n\r\nEkspektasinya, trim terlengkap mendapat HSTC dan ABS, tengah kebagian ABS saja, serta paling standar tak dapat sensor-sensor pengaman. Namun dilengkapi sistem kunci pintar.', 'admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `event`
--

CREATE TABLE `event` (
  `id` int(11) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `photo` varchar(100) NOT NULL,
  `detail` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `event`
--

INSERT INTO `event` (`id`, `judul`, `photo`, `detail`) VALUES
(8, 'Kopi Darat Gabungan Honda PCX Club Indonesia Korwil Jawa Tengah', '608a672caa125_790_480.jpg', 'Dimulai tgl 03/01/2022'),
(9, 'Silver Celebration 25th Year Anniversary Tiger Motor Club Surabaya', '608a64d2d039d_790_480.jpg', 'Dimulai tgl 20/01/2032'),
(10, 'Kompaknya CRF Owner\'s Semarang, Jelajah Offroad & Adventure', '60646ec54ea8e_790_480.jpg', 'Dimulai tgl 20/3/2022'),
(11, 'Touring Gentlemen Ride HPCI Bandungi', 'event3.jpg', 'Dimulai tgl 1/01/2022');

-- --------------------------------------------------------

--
-- Struktur dari tabel `galery`
--

CREATE TABLE `galery` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `photo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `galery`
--

INSERT INTO `galery` (`id`, `nama`, `photo`) VALUES
(2, 'Motor Panjang', '026204800_1525529755-2.jpg'),
(3, 'Modif Baru', '3049078467.jpg'),
(4, 'Kece', 'Modifikasi-Motor-Ceper-Bebek-10.jpg'),
(5, 'Hijau Mempesona', '2508e8ca-6d84-4c9c-a17c-ec8602a663fe.jpeg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `klien`
--

CREATE TABLE `klien` (
  `id` int(11) NOT NULL,
  `photo` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `profesi` varchar(100) NOT NULL,
  `komentar` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `klien`
--

INSERT INTO `klien` (`id`, `photo`, `nama`, `profesi`, `komentar`) VALUES
(2, 'klien.jpeg', 'Paidi', 'CEO Gojek', 'Web ini sangat bagus serta menyediakan produk yang berkualitas'),
(3, 'jeff.jpg', 'Jeff Bezos', 'CEO Amazon', 'Bagus sekali');

-- --------------------------------------------------------

--
-- Struktur dari tabel `produk`
--

CREATE TABLE `produk` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `photo` varchar(100) NOT NULL,
  `deskripsi` varchar(100) NOT NULL,
  `harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `produk`
--

INSERT INTO `produk` (`id`, `nama`, `photo`, `deskripsi`, `harga`) VALUES
(2, 'Ban tipe 5013', 'ban.jfif', 'Dengan Ban yang memiliki banyak rongga sehingga tidak gampang aus', 120000),
(3, 'Skok tipe RGB', 'skok1.jfif', 'Dengan tipe yang bagus sehingga pengendara bisa memilih banyak warna', 300000),
(4, 'Jok Motor', 'jok.jfif', 'Tidak ada', 50000),
(5, 'Knalpot tipe 115', 'knalpot.jpg', 'Knalpot panjang agar tidak mudah kena banjir', 150000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id_login` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_login`, `username`, `email`, `password`) VALUES
(1, 'admin', '', '21232f297a57a5a743894a0e4a801fc3'),
(2, 'blabla', 'blabla@mail.com', 'blabla'),
(3, 'qqqq', 'qqqq@mail.com', '437599f1ea3514f8969f161a6606ce18'),
(4, 'qqqq', 'qqqq@mail.com', '437599f1ea3514f8969f161a6606ce18'),
(5, 'admin', 'admin@admin.com', '21232f297a57a5a743894a0e4a801fc3'),
(6, 'user', 'user@user.com', 'ee11cbb19052e40b07aac0ca060c23ee'),
(7, 'admin1', 'admin@admin.com', 'e00cf25ad42683b3df678c61f42c6bda');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `artikel`
--
ALTER TABLE `artikel`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `galery`
--
ALTER TABLE `galery`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `klien`
--
ALTER TABLE `klien`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_login`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `artikel`
--
ALTER TABLE `artikel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT untuk tabel `event`
--
ALTER TABLE `event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `galery`
--
ALTER TABLE `galery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `klien`
--
ALTER TABLE `klien`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `produk`
--
ALTER TABLE `produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id_login` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
