<?php
session_start();
require '../../koneksi/koneksi.php';




if (!empty($_GET['aksi'] == "tambah")) {
    $data[] =  $_POST["nama"];
    $data[] =  $_FILES["photo"]['name'];
    $data[] =  $_POST["deskripsi"];
    $data[] =  $_POST["harga"];
    $tmp = $_FILES['photo']['tmp_name'];

    // var_dump($data);
    if (move_uploaded_file($tmp, 'image/' . $_FILES["photo"]['name'])) {

        $sql = "INSERT INTO produk (nama,photo,deskripsi,harga ) VALUES ( ?,?,?,?)";
        $row = $koneksi->prepare($sql);
        $row->execute($data);
    }

    echo "<script>window.location='indexproduk.php';</script>";
}

if (!empty($_GET['aksi'] == "edit")) {
    $id =  (int)$_GET["id"];
    $data[] =  $_POST["nama"];
    $data[] =  $_FILES["photo"]['name'];
    $data[] =  $_POST["deskripsi"];
    $data[] =  $_POST["harga"];
    $tmp = $_FILES['photo']['tmp_name'];

    $data[] = $id;
    if (move_uploaded_file($tmp, 'image/' . $_FILES["photo"]['name'])) {
        $sql = "UPDATE produk SET nama = ?, photo = ?, deskripsi = ?, harga = ?  WHERE id = ? ";
        $row = $koneksi->prepare($sql);
        $row->execute($data);
    }
    echo "<script>window.location='indexproduk.php';</script>";
}

if (!empty($_GET['aksi'] == "hapus")) {

    $id =  (int)$_GET["id"]; // should be integer (id)
    $sql = "SELECT * FROM produk WHERE id = ?";
    $row = $koneksi->prepare($sql);
    $row->execute(array($id));
    $cek = $row->rowCount();
    if ($cek > 0) {
        $sql_delete = "DELETE FROM produk WHERE id = ?";
        $row_delete = $koneksi->prepare($sql_delete);
        $row_delete->execute(array($id));
        echo "<script>window.location='indexproduk.php';</script>";
    } else {
        echo "<script>window.location='indexproduk.php';</script>";
    }
}
