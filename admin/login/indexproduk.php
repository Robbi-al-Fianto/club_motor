<?php
// session start
if (!empty($_SESSION)) {
} else {
    session_start();
}
require '../../koneksi/koneksi.php';
if (!empty($_SESSION['ADMIN'])) {
} else {
    echo '<script>alert("Silahkan Login Dahulu !");window.location="login.php"</script>';
}
include 'navbar.php';
?>
<div class="container-fluid position-relative d-flex p-0">

    <!-- Sidebar Start -->
    <div class="sidebar pe-4 pb-3">
        <nav class="navbar bg-secondary navbar-dark">
            <a href="index.html" class="navbar-brand mx-4 mb-3">
                <img src="../image/honda.jpg" class="img-fluid" alt="" width="100px" height="100px">
            </a>
            <div class="d-flex align-items-center ms-4 mb-4">
                <div class="position-relative">
                    <img class="rounded-circle" src="img/user.jpg" alt="" style="width: 40px; height: 40px;">
                    <div class="bg-success rounded-circle border border-2 border-white position-absolute end-0 bottom-0 p-1"></div>
                </div>
                <div class="ms-3">
                    <span>Admin</span>
                </div>
            </div>
            <div class="navbar-nav w-100">
                <a href="index.php" class="nav-item nav-link"><i class="fa fa-th me-2"></i>Artikel</a>
            </div>
            <div class="navbar-nav w-100">
                <a href="indexevent.php" class="nav-item nav-link"><i class="fa fa-keyboard me-2"></i>Event</a>
            </div>
            <div class="navbar-nav w-100">
                <a href="indexgalery.php" class="nav-item nav-link"><i class="fa fa-table me-2"></i>Galery</a>
            </div>
            <div class="navbar-nav w-100">
                <a href="indexklien.php" class="nav-item nav-link"><i class="fa fa-chart-bar me-2"></i>Klien</a>
            </div>
            <div class="navbar-nav w-100">
                <a href="indexproduk.php" class="nav-item nav-link active"><i class="fa-solid fa-box"></i>Produk</a>
            </div>
        </nav>
    </div>
    <!-- Sidebar End -->

    <!-- Content -->
    <div class="container-fluid pt-4 px-4">
        <div class="row g-4">
            <div class="bg-secondary rounded h-100 p-4">
                <h6 class="mb-4">Data Artikel</h6>
                <a class="btn btn-primary btn-md" href="tambahproduk.php" role="button">
                    Tambah
                </a>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Photo</th>
                                <th>Deskripsi</th>
                                <th>Harga</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $sql = "SELECT * FROM produk ORDER BY id DESC";
                            $row = $koneksi->prepare($sql);
                            $row->execute();
                            $hasil = $row->fetchAll(PDO::FETCH_OBJ);
                            foreach ($hasil as $r) {
                            ?>
                                <tr>
                                    <td><?= $no; ?></td>
                                    <td><?= $r->nama; ?></td>
                                    <td> <img src="image/<?php echo $r->photo; ?>" class="img-fluid" width="200px;"> </td>
                                    <td><?= $r->deskripsi; ?></td>
                                    <td><?= $r->harga; ?></td>
                                    <td>
                                        <a href="<?= "editproduk.php?id=" . $r->id; ?>" class="btn btn-success btn-sm" title="Edit">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a href="prosesproduk.php?aksi=hapus&id=<?= $r->id; ?>" class="btn btn-danger btn-sm" onclick="javascript:return confirm(`Data ingin dihapus ?`);" title="Delete">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php $no++;
                            } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Content End-->

<?php include 'footer.php'; ?>