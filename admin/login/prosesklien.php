<?php
session_start();
require '../../koneksi/koneksi.php';




if (!empty($_GET['aksi'] == "tambah")) {
    $data[] =  $_FILES["photo"]['name'];
    $data[] =  $_POST["nama"];
    $data[] =  $_POST["profesi"];
    $data[] =  $_POST["komentar"];
    $tmp = $_FILES['photo']['tmp_name'];

    // var_dump($data);
    if (move_uploaded_file($tmp, 'image/' . $_FILES["photo"]['name'])) {

        $sql = "INSERT INTO klien (photo,nama,profesi,komentar ) VALUES ( ?,?,?,?)";
        $row = $koneksi->prepare($sql);
        $row->execute($data);
    }

    echo "<script>window.location='indexklien.php';</script>";
}

if (!empty($_GET['aksi'] == "edit")) {
    $id =  (int)$_GET["id"];
    $data[] =  $_FILES["photo"]['name'];
    $data[] =  $_POST["nama"];
    $data[] =  $_POST["profesi"];
    $data[] =  $_POST["komentar"];
    $tmp = $_FILES['photo']['tmp_name'];

    $data[] = $id;
    if (move_uploaded_file($tmp, 'image/' . $_FILES["photo"]['name'])) {
        $sql = "UPDATE klien SET photo = ?, nama = ?, profesi = ?, komentar = ?  WHERE id = ? ";
        $row = $koneksi->prepare($sql);
        $row->execute($data);
    }
    echo "<script>window.location='indexklien.php';</script>";
}

if (!empty($_GET['aksi'] == "hapus")) {

    $id =  (int)$_GET["id"]; // should be integer (id)
    $sql = "SELECT * FROM klien WHERE id = ?";
    $row = $koneksi->prepare($sql);
    $row->execute(array($id));
    $cek = $row->rowCount();
    if ($cek > 0) {
        $sql_delete = "DELETE FROM klien WHERE id = ?";
        $row_delete = $koneksi->prepare($sql_delete);
        $row_delete->execute(array($id));
        echo "<script>window.location='indexklien.php';</script>";
    } else {
        echo "<script>window.location='indexklien.php';</script>";
    }
}
