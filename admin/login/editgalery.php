<?php
// session start
// session cek login
if (!empty($_SESSION)) {
} else {
    session_start();
}
require '../../koneksi/koneksi.php';
if (!empty($_SESSION['ADMIN'])) {
} else {
    echo '<script>alert("Silahkan Login Dahulu !");window.location="login.php"</script>';
}
include 'navbar.php';
$id =  (int)$_GET["id"];
$sql = "SELECT * FROM galery WHERE id = ?";
$row = $koneksi->prepare($sql);
$row->execute(array($id));
$edit = $row->fetch(PDO::FETCH_OBJ);
?>
<div class="container-fluid position-relative d-flex p-0">

    <!-- Sidebar Start -->
    <div class="sidebar pe-4 pb-3">
        <nav class="navbar bg-secondary navbar-dark">
            <a href="index.html" class="navbar-brand mx-4 mb-3">
                <img src="../image/honda.jpg" class="img-fluid" alt="" width="100px" height="100px">
            </a>
            <div class="d-flex align-items-center ms-4 mb-4">
                <div class="position-relative">
                    <img class="rounded-circle" src="img/user.jpg" alt="" style="width: 40px; height: 40px;">
                    <div class="bg-success rounded-circle border border-2 border-white position-absolute end-0 bottom-0 p-1"></div>
                </div>
                <div class="ms-3">
                    <span>Admin</span>
                </div>
            </div>
            <div class="navbar-nav w-100">
                <a href="index.php" class="nav-item nav-link"><i class="fa fa-th me-2"></i>Artikel</a>
            </div>
            <div class="navbar-nav w-100">
                <a href="indexevent.php" class="nav-item nav-link"><i class="fa fa-keyboard me-2"></i>Event</a>
            </div>
            <div class="navbar-nav w-100">
                <a href="indexgalery.php" class="nav-item nav-link active"><i class="fa fa-table me-2"></i>Galery</a>
            </div>
            <div class="navbar-nav w-100">
                <a href="indexklien.php" class="nav-item nav-link"><i class="fa fa-chart-bar me-2"></i>Klien</a>
            </div>
            <div class="navbar-nav w-100">
                <a href="indexproduk.php" class="nav-item nav-link"><i class="fa-solid fa-box"></i>Produk</a>
            </div>
        </nav>
    </div>
    <!-- Sidebar End -->



    <!-- Form Edit -->
    <div class="container-fluid pt-4 px-4">
        <div class="row g-4">
            <div class="col-12">
                <div class="bg-secondary rounded h-100 p-4">
                    <h6 class="mb-4">Floating Label</h6>
                    <form method="post" enctype="multipart/form-data" action="prosesgalery.php?aksi=edit&id=<?= $id; ?>">
                        <div class="form-floating mb-3">
                            <input type="text" class="form-control" value="<?= $edit->nama; ?>" name="nama" id="nama" placeholder="input judul">
                            <label for="floatingInput">Nama</label>
                        </div>
                        <div class="mb-3">
                            <label for="formFile" class="form-label">Photo</label>
                            <input class="form-control bg-dark" type="file" value="<?= $edit->photo; ?>" name="photo" id="photo">
                        </div>
                        <div class="form-floating">
                            <button type="submit" class="btn btn-primary btn-md btn-block">
                                Edit
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- End Form Edit -->
</div>


<?php include 'footer.php'; ?>