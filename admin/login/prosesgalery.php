<?php
session_start();
require '../../koneksi/koneksi.php';




if (!empty($_GET['aksi'] == "tambah")) {
    $data[] =  $_POST["nama"];
    $data[] =  $_FILES["photo"]['name'];
    $tmp = $_FILES['photo']['tmp_name'];

    // var_dump($data);
    if (move_uploaded_file($tmp, 'image/' . $_FILES["photo"]['name'])) {

        $sql = "INSERT INTO galery (nama,photo ) VALUES ( ?,?)";
        $row = $koneksi->prepare($sql);
        $row->execute($data);
    }

    echo "<script>window.location='indexgalery.php';</script>";
}

if (!empty($_GET['aksi'] == "edit")) {
    $id =  (int)$_GET["id"];
    $data[] =  $_POST["nama"];
    $data[] =  $_FILES["photo"]['name'];
    $tmp = $_FILES['photo']['tmp_name'];

    $data[] = $id;
    if (move_uploaded_file($tmp, 'image/' . $_FILES["photo"]['name'])) {
        $sql = "UPDATE galery SET nama = ?, photo = ?  WHERE id = ? ";
        $row = $koneksi->prepare($sql);
        $row->execute($data);
    }
    echo "<script>window.location='indexgalery.php';</script>";
}

if (!empty($_GET['aksi'] == "hapus")) {

    $id =  (int)$_GET["id"]; // should be integer (id)
    $sql = "SELECT * FROM galery WHERE id = ?";
    $row = $koneksi->prepare($sql);
    $row->execute(array($id));
    $cek = $row->rowCount();
    if ($cek > 0) {
        $sql_delete = "DELETE FROM galery WHERE id = ?";
        $row_delete = $koneksi->prepare($sql_delete);
        $row_delete->execute(array($id));
        echo "<script>window.location='indexgalery.php';</script>";
    } else {
        echo "<script>window.location='indexgalery.php';</script>";
    }
}
