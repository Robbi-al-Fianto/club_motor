<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>DarkPan - Bootstrap 5 Admin Template</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Favicon -->
    <link href="img/favicon.ico" rel="icon">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600&family=Roboto:wght@500;700&display=swap" rel="stylesheet">

    <!-- Icon Font Stylesheet -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

    <!-- Libraries Stylesheet -->
    <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="lib/tempusdominus/css/tempusdominus-bootstrap-4.min.css" rel="stylesheet" />

    <!-- Customized Bootstrap Stylesheet -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Template Stylesheet -->
    <link href="css/style.css" rel="stylesheet">


</head>

<body>
    <div class="container-fluid position-relative d-flex p-0">
        <!-- Spinner Start -->
        <div id="spinner" class="show bg-dark position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
            <div class="spinner-border text-primary" style="width: 3rem; height: 3rem;" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
        <!-- Spinner End -->


        <!-- Sidebar Start -->
        <div class="sidebar pe-4 pb-3">
            <nav class="navbar bg-secondary navbar-dark">
                <a href="index.php" class="navbar-brand mx-4 mb-3">
                    <img src="image/honda.jpg" class="img-fluid" alt="" width="100px" height="100px">
                </a>
                <div class="navbar-nav w-100">
                    <a href="index.php" class="nav-item nav-link active"><i class="fa fa-tachometer-alt me-2"></i>Home</a>
                    <a href="artikelsidebar.php" class="nav-item nav-link"><i class="fa fa-th me-2"></i>Artikel</a>
                    <a href="eventsidebar.php" class="nav-item nav-link"><i class="fa fa-keyboard me-2"></i>Event</a>
                    <a href="galerysidebar.php" class="nav-item nav-link"><i class="fa fa-table me-2"></i>Galery Foto</a>
                    <a href="kliensidebar.php" class="nav-item nav-link"><i class="fa fa-chart-bar me-2"></i>Klien kami</a>
                    <div class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown"><i class="far fa-file-alt me-2"></i>Pages</a>
                        <div class="dropdown-menu bg-transparent border-0">
                            <a href="login/login.php" class="dropdown-item">Sign In</a>
                            <a href="login/signup.php" class="dropdown-item">Sign Up</a>
                            <a href="404.html" class="dropdown-item">404 Error</a>
                            <a href="blank.html" class="dropdown-item">Blank Page</a>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
        <!-- Sidebar End -->


        <!-- Content Start -->
        <div class="content">
            <!-- Navbar Start -->
            <nav class="navbar navbar-expand bg-secondary navbar-dark sticky-top px-4 py-0">
                <a href="index.php" class="navbar-brand d-flex d-lg-none me-4">
                    <h2 class="text-primary mb-0"><i class="fa fa-user-edit"></i></h2>
                </a>
                <a href="#" class="sidebar-toggler flex-shrink-0">
                    <i class="fa fa-bars"></i>
                </a>
                <div class="navbar-nav align-items-center ms-auto">
                    <div class="nav-item">
                        <a href="#" class="nav-link">

                            <span class="d-none d-lg-inline-flex">Home</span>
                        </a>
                    </div>
                    <div class="nav-item">
                        <a href="profiletopbar.php" class="nav-link">

                            <span class="d-none d-lg-inline-flex">Profile</span>
                        </a>
                    </div>
                    <div class="nav-item">
                        <a href="visimisitopbar.php" class="nav-link">

                            <span class="d-none d-lg-inline-flex">Visi Misi</span>
                        </a>
                    </div>
                    <div class="nav-item">
                        <a href="produktopbar.php" class="nav-link">

                            <span class="d-none d-lg-inline-flex">Produk Kami</span>
                        </a>
                    </div>
                    <div class="nav-item">
                        <a href="kontaktopbar.php" class="nav-link">

                            <span class="d-none d-lg-inline-flex">Kontak Kami</span>
                        </a>
                    </div>
                    <div class="nav-item">
                        <a href="aboutustopbar.php" class="nav-link">

                            <span class="d-none d-lg-inline-flex">About Us</span>
                        </a>
                    </div>
                </div>
            </nav>
            <!-- Navbar End -->

            <!-- Sale & Revenue Start -->

            <div class="container-fluid pt-4 px-4">
                <div class="row g-4">
                    <div class="col-12 col-xl-8">
                        <div class="bg-secondary rounded h-100 p-4">
                            <h1 class="text-white">PT Integrated Web Honda Community</h1>
                            <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        <img src="image/oneheart.png" class="d-block w-100" alt="...">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-xl-4">
                        <div class="h-100 bg-secondary rounded p-4">
                            <h1 class="text-white">Kata Pengantar</h1>
                            <p>Selamat Datang

                                di website PT Integrated Web Honda Community


                            <p>Assalamu'alaikum Wr. Wb.</p>

                            Puji syukur kami panjatkan kehadirat ALLAH SWT atas limpahan rahmat dan karunia-Nya sehingga PT Integrated Web Honda Community berhasil membangun website, Kehadiran Website PT Integrated Web Honda Community diharapkan dapat memudahkan penyampaian informasi secara terbuka kepada masyarakat umum serta instansi lain yang terkait.

                            Semoga dengan kehadiran Website ini akan terjalin informasi, komunikasi antar komunitas khususnya sebagai salah satu upaya mendapatkan informasi akan penelusuran event event dimana saja berada. Dapat memperoleh informasi dengan cepat sehingga dapat mengikuti perkembangan dalam sillaturahmi akan lebih baik.

                            <p>Selamat bekerja,</p>

                            Demikian dan terima kasih.
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Sale & Revenue End -->


            <!-- Sales Chart Start -->


            <!-- Sales Chart End -->



            <!-- Recent Sales Start -->

            <!-- Recent Sales End -->


            <!-- Widgets Start -->

            <!-- Widgets End -->


            <!-- Footer Start -->
            <div class="container-fluid pt-4 px-4">
                <div class="bg-secondary rounded-top p-4">
                    <div class="row">
                        <div class="col-12 col-sm-6 text-center text-sm-start">
                            &copy; <a href="#">Your Site Name</a>, All Right Reserved.
                        </div>
                        <div class="col-12 col-sm-6 text-center text-sm-end">
                            <!--/*** This template is free as long as you keep the footer author’s credit link/attribution link/backlink. If you'd like to use the template without the footer author’s credit link/attribution link/backlink, you can purchase the Credit Removal License from "https://htmlcodex.com/credit-removal". Thank you for your support. ***/-->
                            Designed By <a href="https://htmlcodex.com">HTML Codex</a>
                            <br>Distributed By: <a href="https://themewagon.com" target="_blank">ThemeWagon</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer End -->
        </div>
        <!-- Content End -->


        <!-- Back to Top -->
        <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>
    </div>

    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="lib/chart/chart.min.js"></script>
    <script src="lib/easing/easing.min.js"></script>
    <script src="lib/waypoints/waypoints.min.js"></script>
    <script src="lib/owlcarousel/owl.carousel.min.js"></script>
    <script src="lib/tempusdominus/js/moment.min.js"></script>
    <script src="lib/tempusdominus/js/moment-timezone.min.js"></script>
    <script src="lib/tempusdominus/js/tempusdominus-bootstrap-4.min.js"></script>

    <!-- Template Javascript -->
    <script src="js/main.js"></script>
</body>

</html>